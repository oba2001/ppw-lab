from django.shortcuts import render, HttpResponseRedirect
from .forms import KegiatanForm, PesertaForm
from .models import Kegiatan, Peserta

# Create your views here.
def index(request):
    context = {
        'KEGIATAN' : Kegiatan.objects.all(),
        'PESERTA' : Peserta.objects.all(),
        'formK' : KegiatanForm(),
        'formP' : PesertaForm()
    } 
    return render(request, 'index.html', context)

def kegiatan(request):
    if request.method == 'POST':
        form = KegiatanForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/kegiatan')
    return HttpResponseRedirect('/kegiatan')

def peserta(request, idKegiatan):
    if request.method == 'POST':
        form = PesertaForm(request.POST)
        if form.is_valid():
            id_kegiatan = Kegiatan.objects.get(id=idKegiatan)
            dataForm = form.cleaned_data
            dataMasuk = Peserta()
            dataMasuk.namaPeserta = dataForm['namaPeserta']
            dataMasuk.kegiatans = id_kegiatan
            dataMasuk.save()
            return HttpResponseRedirect('/kegiatan')
    return HttpResponseRedirect('/kegiatan')