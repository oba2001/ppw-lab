from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .models import Kegiatan, Peserta

# Create your tests here.
class TestModelRelation(TestCase):
    def test_url_landing_page(self):
        response = Client().get('/kegiatan')
        self.assertEqual(200, response.status_code)

    def test_template_used_for_landing_page(self):
        response = Client().get('/kegiatan')
        self.assertTemplateUsed(response ,'index.html')

    def test_func_used_for_landing_page(self):
        response = resolve('/kegiatan')
        self.assertEqual(response.func, views.index)
        
    def test_can_save_a_peserta_POST(self):
        kegiatan = Kegiatan.objects.create(namaKegiatan='Kegiatan1')
        a = Client().post('/kegiatan/addPeserta/' + str(kegiatan.id), data={'namaPeserta':'Nama1'})
        count = Peserta.objects.all().count()
        namaPeserta = Peserta.objects.get(namaPeserta='Nama1')
        self.assertEqual(count, 1)
        self.assertEqual(str(namaPeserta), "Nama1")
        self.assertEqual(a.status_code, 302)

    def test_kegiatan_model(self):
        a = Kegiatan.objects.create(namaKegiatan='Kegiatan2')
        namaKegiatan = Kegiatan.objects.get(namaKegiatan='Kegiatan2')
        self.assertEqual(str(namaKegiatan), "Kegiatan2")

    def test_invalid_form_kegiatan(self):
        a = Client().post('/kegiatan/addKegiatan', data={})
        jumlah = Kegiatan.objects.all().count()
        self.assertEqual(jumlah, 0)
        self.assertEqual(a.status_code, 302)