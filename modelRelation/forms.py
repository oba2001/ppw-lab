from django import forms

from .models import Peserta, Kegiatan


class PesertaForm(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = [
            'namaPeserta',
        ]
        widgets = {
            'namaPeserta': forms.TextInput(
                attrs = {
                    'class':'form-control',
                    'placeholder':'Nama peserta'
                }
            ),
        }

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = [
            'namaKegiatan',
        ]
        widgets = {
            'namaKegiatan': forms.TextInput(
                attrs = {
                    'class':'form-control',
                    'placeholder':'Nama kegiatan'
                }
            ),
        }