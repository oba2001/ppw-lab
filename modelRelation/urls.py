from django.urls import path
from . import views
app_name = 'modelRelation'

urlpatterns = [
    path('', views.index, name='index'),
    path('/addKegiatan', views.kegiatan, name='kegiatan'),
    path('/addPeserta/<int:idKegiatan>', views.peserta, name='peserta')
      
    ]