from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    namaKegiatan = models.CharField(max_length=200)

    def __str__(self):
        return self.namaKegiatan

class Peserta(models.Model):
    namaPeserta = models.CharField(max_length = 200)
    kegiatans = models.ForeignKey(Kegiatan, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.namaPeserta