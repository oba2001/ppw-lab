$('#inputText').keyup( function(){
    var kunci = $('#inputText').val();
    console.log(kunci);

    $.ajax({
        url: 'https://www.googleapis.com/books/v1/volumes?q=' + kunci,
        success: function(data){
          var array_items = data.items;
          console.log(array_items);
          $('#hasil').empty();
          for (i=0; i<array_items.length; i++){
            var gambar = array_items[i].volumeInfo.imageLinks.smallThumbnail;
            var judul = array_items[i].volumeInfo.title;
            var penerbit = array_items[i].publisher;
            $('hasil').append("<li>" + "<img src=" + gambar + ">" + judul + "<br>" + penerbit +"</li>");
          }
        }
    });
});