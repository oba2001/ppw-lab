from django.test import TestCase, Client
from django.urls import resolve
from . import views
from django.contrib.auth.models import User

# Create your tests here.
class TestDaftarBuku(TestCase):
    def test_url(self):
        response = Client().get('/daftar-buku')
        self.assertEquals(200, response.status_code)
    def test_url_regist(self):
        response = Client().get('/register')
        self.assertEquals(200, response.status_code)

    def test_template_used(self):
        response = Client().get('/daftar-buku')
        self.assertTemplateUsed(response, 'daftar_buku.html')

    def test_view_func(self):
        response = resolve('/daftar-buku')
        self.assertEquals(response.func, views.index)

    def test_pemanggilan_tautan(self):
        response = Client().get('/data?q=start%202')
        self.assertEqual(response.status_code, 200)

    def test_regist_func(self):
        response = resolve('/register')
        self.assertEquals(response.func, views.registrasi)

    def test_regist_template(self):
        response = Client().get('/register')
        self.assertTemplateUsed(response, 'halaman_register.html')

    def test_login_url(self):
        response = Client().get('/login')
        self.assertEquals(response.status_code, 200)

    def test_login_template(self):
        response = Client().get('/login')
        self.assertTemplateUsed(response, 'halaman_login.html')

    def test_logout_url(self):
        response = Client().get('/logout')
        self.assertEquals(response.status_code, 302)

    def test_user_added(self):
        User.objects.create(username="ani", email="ani@gmail.com" )
        jumlah = User.objects.all().count()
        self.assertEquals(1, jumlah)
        nama = User.objects.get(username="ani")
        self.assertEquals("ani", str(nama))

    def test_login(self):
        self.credentials = {
            'username' : 'coba',
            'password' : 'ayok1212',
        }
        User.objects.create_user(**self.credentials)
        response = self.client.post('/login', self.credentials, follow=True)
        self.assertTrue(response.context['user'].is_active)

    def test_login_error(self):
        self.credentials = {
            'username' : 'coba',
            'password' : 'ayok1212',
        }
        User.objects.create_user(**self.credentials)
        error = {
            'username' : 'coba',
            'password' : 'ayok1312',
        }
        response = self.client.post('/login', error, follow=True)
        self.assertFalse(response.context['user'].is_active)

   