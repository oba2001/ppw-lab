from django.shortcuts import render, HttpResponseRedirect
from django.http import JsonResponse, HttpResponse
from django.forms import inlineformset_factory
import requests
import json
from .forms import RegisterForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):
    return render(request, 'daftar_buku.html')

def data_func(request):
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + request.GET['q']
    ret = requests.get(url)
    data = json.loads(ret.content)

    if not request.user.is_authenticated:
        i = 0
        for x in data['items']:
            del data['items'][i]['volumeInfo']['imageLinks']['smallThumbnail']
            i = i + 1

    return JsonResponse(data, safe=False)

def registrasi(request):
    form = RegisterForm()

    if request.method == 'POST':
        form = RegisterForm(request.POST )
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get('username')
            messages.success(request, 'Account was created successfully! Please login to continue!')
            return HttpResponseRedirect('login')
    context = {'reg_form':form}
    return render(request, 'halaman_register.html', context)

def loginFunc(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username = username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                request.session['username'] = username
                return HttpResponseRedirect('daftar-buku')

    return render(request, 'halaman_login.html')

@login_required
def logoutFunc(request):
    if request.session.has_key('nama'):
        request.session.flush()
        logout(request)
        return HttpResponseRedirect('daftar-buku')
    logout(request)
    return HttpResponseRedirect('daftar-buku')
