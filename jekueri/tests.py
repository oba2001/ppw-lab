from django.test import TestCase, Client, LiveServerTestCase, tag
from django.urls import resolve
from . import views
# from selenium import webdriver

# # Create your tests here.
# @tag('functional')
# class FunctionalTestCase(LiveServerTestCase):
#     """Base class for functional test cases with selenium."""
#     @classmethod
#     def setUpClass(cls):
#         super().setUpClass()
#         # Change to another webdriver if desired (and update CI accordingly).
#         options = webdriver.chrome.options.Options()
#         # These options are needed for CI with Chromium.
#         options.headless = True  # Disable GUI.
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         cls.selenium = webdriver.Chrome(options=options)
    
#     @classmethod
#     def tearDownClass(cls):
#         cls.selenium.quit()
#         super().tearDownClass()

class TestJekueri(TestCase):
    def test_url(self):
        response = Client().get('/task-7')
        self.assertEquals(200, response.status_code)
    
    def test_template_used(self):
        response = Client().get('/task-7')
        self.assertTemplateUsed(response, 'home.html')

    def test_view_func(self):
        response = resolve('/task-7')
        self.assertEquals(response.func, views.index)

# class MainFunctionalTestCase(FunctionalTestCase):
#     def test_root_url_exists(self):
#         self.selenium.get(f'{self.live_server_url}/')
#         html = self.selenium.find_element_by_tag_name('html')
#         self.assertNotIn('not found', html.text.lower())
#         self.assertNotIn('error', html.text.lower())