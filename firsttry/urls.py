"""firsttry URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from daftarBuku.views import data_func, registrasi, loginFunc, logoutFunc


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('story4.urls')),
    path('story1', include('home.urls')),
    path('task-7', include('jekueri.urls')),
    path('daftar-buku', include('daftarBuku.urls')),
    path('data', data_func, name='data'),    
    path('register', registrasi, name='register'),
    path('login', loginFunc, name='login'),
    path('logout', logoutFunc, name='logout'),
    path('kegiatan', include('modelRelation.urls')),

]
