from django.db import models


# Create your models here.


class DaftarMatkul(models.Model):
    namaMK = models.CharField(max_length = 200)
    dosen = models.CharField(max_length = 200)
    T_CHOICES = (
        ('2019-2020 (Ganjil)','2019-2020 (Ganjil)'),
        ('2019-2020 (Genap)','2019-2020 (Genap)'),
        ('2020-2021 (Ganjil)','2020-2021 (Ganjil)'),
        ('2020-2021 (Genap)','2020-2021 (Genap)'),
        ('2021-2022 (Ganjil)','2021-2022 (Ganjil)'),
        ('2021-2022 (Genap)','2021-2022 (Genap)'),
        ('2022-2023 (Ganjil)','2022-2023 (Ganjil)'),
        ('2022-2023 (Genap)','2022-2023 (Genap)')
    )
    SKS_CHOICES = (('1','1'),('2','2'),('3','3'),('4','4'),('5','5'),('6','6')
    )
    sks = models.CharField(max_length = 20, choices=SKS_CHOICES,default='3')
    deskripsi = models.CharField(max_length = 2000)
    tahunAjaran = models.CharField(max_length = 100, choices=T_CHOICES, default='2020-2021 (Ganjil)')
    ruang = models.CharField(max_length = 200)

    def __str__(self):
        return "{}.{}".format(self.id, self.namaMK)