from django.shortcuts import (render, get_object_or_404, HttpResponseRedirect)
from .forms import DaftarMatkulForm
from .models import DaftarMatkul


# Create your views here.
def home(request):
    return render(request, 'story4/index.html')
def about(request):
    return render(request, 'story4/about.html')
def bio(request):
    return render(request, 'story4/bio.html')
def edu(request):
    return render(request, 'story4/edu.html')
def contact(request):
    return render(request, 'story4/contact.html')
def expe(request):
    return render(request, 'story4/expe.html')
def add(request):
    return render(request, 'story4/music.html')

def daftar_matkul_create(request):
    matkul_form = DaftarMatkulForm(request.POST or None)

    if request.method == 'POST':
        if matkul_form.is_valid():
            matkul_form.save()
            return HttpResponseRedirect('daftar')

    context = {
        'page_title': 'Create Post',
        'matkul_form': matkul_form,
    }
    return render(request, 'story4/matkul_form.html', context )

def daftar(request):
    posts = DaftarMatkul.objects.all()

    context = {
        'page_title':'Semua Post',
        'posts':posts
    }
    return render(request, 'story4/matkul_list.html', context)
def delete(request, nama):
    data = DaftarMatkul.objects.filter(id=nama)
    data.delete()
    return HttpResponseRedirect('/daftar')
def detail(request, pk):
    detail = DaftarMatkul.objects.get(id=pk)
    context={
        'post' : detail
    }
    return render(request, 'story4/detail.html', context)