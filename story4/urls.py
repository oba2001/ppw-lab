from django.urls import path
from . import views
app_name = 'story4'

urlpatterns = [
    path('', views.home, name='Home'),
    path('About', views.about, name='About'),
    path('Home', views.home, name='Home'),
    path('Bio', views.bio, name='bio'),
    path('Edu', views.edu, name='edu'),
    path('Contact', views.contact, name='contact'),
    path('Expe', views.expe, name='expe'),
    path('addition', views.add, name='add'),
    path('matkul', views.daftar_matkul_create, name='matkul'),
    path('daftar', views.daftar, name="daftar"),
    path('delete/<nama>', views.delete, name='delete'),
    path('detail/<pk>', views.detail, name='detail')
]