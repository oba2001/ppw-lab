from django import forms

from .models import DaftarMatkul


class DaftarMatkulForm(forms.ModelForm):
    class Meta:
        model = DaftarMatkul
        fields = [
            'namaMK',
            'dosen',
            'sks',
            'deskripsi',
            'tahunAjaran',
            'ruang',
        ]
        widgets = {
            'namaMK': forms.TextInput(
                attrs = {
                    'class':'form-control',
                    'placeholder':'nama mata kuliah'
                }
            ),
            'dosen': forms.TextInput(
                attrs = {
                    'class':'form-control',
                    'placeholder':'nama dosen'
                }
            ),
            'sks': forms.Select(
                attrs = {
                    'class':'form-control',
                }
            ),
            'deskripsi': forms.Textarea(
                attrs = {
                    'class':'form-control',
                    'placeholder':'deskripsikan mata kuliah'
                }
            ),
            'tahunAjaran': forms.Select(
                attrs = {
                    'class':'form-control',
                }
            ),
            'ruang': forms.TextInput(
                attrs = {
                    'class':'form-control',
                    'placeholder':'ruangan perkuliahan'
                }
            )
        }
        
